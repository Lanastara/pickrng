use std::net::SocketAddr;

use axum::{
    routing::{get, post},
    Router,
};
use axum_session::{SessionConfig, SessionLayer, SessionStore, SessionSurrealPool};
use axum_session_auth::{AuthConfig, AuthSessionLayer};
use clap::Parser;
use rand::Rng;
use surrealdb::{
    engine::any::{connect, Any},
    Surreal,
};

mod api;
mod auth;
mod data;

use crate::auth::User;

#[derive(Parser)]
struct Options {
    #[clap(env, default_value = "0.0.0.0:8080")]
    server_address: SocketAddr,
    #[clap(env, long)]
    database_url: String,
}

#[derive(Clone)]
struct AppState {
    pool: Surreal<Any>,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenvy::dotenv()?;
    let ops = Options::parse();

    let surreal = connect(ops.database_url).await?;

    surreal.use_ns("pickrng").use_db("pickrng").await?;

    tracing_subscriber::fmt::init();

    let mut secret = [0u8; 64];
    rand::thread_rng().fill(&mut secret);
    let session_config = SessionConfig::default().with_session_name("session");
    let auth_config = AuthConfig::<String>::default().with_anonymous_user_id(Some(String::new()));
    let session_store =
        SessionStore::<SessionSurrealPool<_>>::new(Some(surreal.clone().into()), session_config)
            .await?;

    let app = Router::new()
        .route("/dice/roll", get(api::dice::roll))
        .route("/card/add", post(api::deck::add_card))
        .route_layer(axum::middleware::from_fn(auth::logged_in_middleware))
        .route("/login", get(auth::login))
        .route("/logout", get(auth::logout))
        .route("/register", get(auth::register))
        .layer(
            AuthSessionLayer::<User, String, SessionSurrealPool<Any>, Surreal<Any>>::new(Some(
                surreal.clone(),
            ))
            .with_config(auth_config),
        )
        .layer(SessionLayer::new(session_store))
        .with_state(AppState { pool: surreal });

    tracing::debug!("Listening on {}", ops.server_address);

    axum::Server::bind(&ops.server_address)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
