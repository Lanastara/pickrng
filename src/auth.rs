use axum::{
    async_trait,
    extract::{Query, State},
    middleware::Next,
    response::Response,
};
use axum_session::SessionSurrealPool;
use axum_session_auth::{Authentication};
use http::{Request, StatusCode};
use rand::rngs::OsRng;
use scrypt::{
    password_hash::{PasswordHasher, SaltString},
    Scrypt,
};
use serde::{Deserialize, Serialize};
use surrealdb::{Surreal, engine::any::Any};
use tracing::{error, info, warn};

use crate::AppState;

type AuthSession = axum_session_auth::AuthSession<User, String, SessionSurrealPool<Any>, Surreal<Any>>;

#[derive(Clone, Default, Serialize, Deserialize)]
pub(crate) struct User {
    username: String,
    password: String,
    salt: String,
}

#[async_trait]
impl Authentication<User, String, Surreal<Any>> for User {
    async fn load_user(userid: String, pool: Option<&Surreal<Any>>) -> Result<User, anyhow::Error> {
        let Some(pool) = pool else {
            return Err(anyhow::anyhow!("Missing DB Pool"));
        };

        User::get(&userid, pool)
            .await
            .ok_or_else(|| anyhow::anyhow!("Could not get User"))
    }

    fn is_authenticated(&self) -> bool {
        todo!()
    }

    fn is_active(&self) -> bool {
        todo!()
    }

    fn is_anonymous(&self) -> bool {
        todo!()
    }
}


impl User {
    async fn get(username: &str, pool: &Surreal<Any>) -> Option<Self> {
        pool.select(("user", username)).await.ok()?
    }
    async fn login(
        username: &str,
        password: &str,
        pool: &Surreal<Any>,
    ) -> Result<Option<String>, anyhow::Error> {
        let Some(user) = User::get(username, pool).await else {
            return Ok(None);
        };

        let salt = SaltString::from_b64(&user.salt)?;
        let password_hash = Scrypt
            .hash_password(password.as_bytes(), &salt)?
            .serialize();

        let password_hash = password_hash.as_str();
        if password_hash == user.password {
            Ok(Some(username.to_string()))
        } else {
            Ok(None)
        }
    }

    async fn create(username: &str, password: &str, pool: &Surreal<Any>) -> Result<Option<String>, anyhow::Error> {
        let salt_string = SaltString::generate(&mut OsRng);
        let salt = salt_string.as_str();
        let password_hash = Scrypt.hash_password(password.as_bytes(), &salt_string)?.serialize();

        let password_hash = password_hash.as_str();

        let user = User { username: username.to_string(), password: password_hash.to_string(), salt: salt.to_string() };
        
        let x : Option<User> = pool.create(("user", username)).content(user).await?;

        Ok(x.map(|x|x.username))
    }
}

#[derive(Deserialize)]
pub(crate) struct LoginData {
    username: String,
    password: String,
}

pub(crate) async fn login(
    Query(LoginData { username, password }): Query<LoginData>,
    State(AppState { pool }): State<AppState>,
    auth: AuthSession,
) -> Result<(), StatusCode> {
    match User::login(&username, &password, &pool).await {
        Ok(Some(id)) => {
            info!(username, "User logged In!");
            auth.login_user(id);
            Ok(())
        }
        Ok(None) => Err(StatusCode::UNAUTHORIZED),
        Err(_) => todo!(),
    }
}

pub(crate) async fn logout(auth: AuthSession) {
    auth.logout_user();
}

#[derive(Deserialize)]
pub(crate) struct RegisterData {
    username: String,
    password: String,
    invite_code: String,
}

pub(crate) async fn register(
    Query(RegisterData {
        username,
        password,
        invite_code,
    }): Query<RegisterData>,
    State(AppState { pool }): State<AppState>,
    auth: AuthSession,
) -> Result<(), StatusCode> {
    let root_invite_code = std::env::var("ROOT_INVITE_CODE").ok();

    if password.is_empty() {
        return Err(StatusCode::BAD_REQUEST);
    }

    if username == "root" {
        let Some(root_code) = root_invite_code else  
        {
            return Err(StatusCode::UNAUTHORIZED);
        };
        if invite_code != root_code{
            return Err(StatusCode::UNAUTHORIZED);
        }
    } else {
        todo!("Check Invite Code")
    }

    match User::create(&username, &password, &pool).await {
        Ok(Some(id)) => {
            auth.login_user(id);
            Ok(())
        },
        Ok(None) => {
            Err(StatusCode::UNAUTHORIZED)
        }
        Err(err) => {
            error!(?err);
            Err(StatusCode::INTERNAL_SERVER_ERROR)
        }
    }

}

pub(crate) async fn logged_in_middleware<B>(
    auth: AuthSession, request: Request<B>, next: Next<B>,
) -> Response {

    if auth.current_user.is_none(){
        warn!("Unauthorizec access");
        return Response::builder().status(StatusCode::UNAUTHORIZED).body(Default::default()).unwrap();
    }

    next.run(request).await
}

