use axum::Json;
use axum_extra::extract::Query;
use caith::Roller;
use http::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::warn;

#[derive(Debug, Deserialize)]
#[serde(try_from = "String")]
pub(crate) struct Dice(Roller);

impl TryFrom<String> for Dice {
    type Error = caith::RollError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Roller::new(&value).map(Dice)
    }
}

#[derive(Debug, Deserialize)]
pub(crate) struct RollQuery {
    dice: Dice,
}

#[derive(Debug, Serialize)]
pub(crate) struct RollResult {
    dice: Vec<String>,
    total: Option<i64>,
}

pub(crate) async fn roll(
    Query(RollQuery { dice: Dice(roller) }): Query<RollQuery>,
) -> Result<Json<RollResult>, StatusCode> {
    let result = roller.roll().map_err(|err| {
        warn!(?err);
        StatusCode::INTERNAL_SERVER_ERROR
    })?;
    let result = result.get_result();

    Ok(Json(match result {
        caith::RollResultType::Single(single) => RollResult {
            total: Some(single.get_total()),
            dice: vec![single.to_string_history()],
        },
        caith::RollResultType::Repeated(repeated) => RollResult {
            total: repeated.get_total(),
            dice: repeated
                .iter()
                .map(|single| single.to_string_history())
                .collect(),
        },
    }))
}
