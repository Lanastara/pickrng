use axum::{extract::State, Json};
use http::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::warn;

use crate::{
    data::{Card, Identified},
    AppState,
};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct CardParams {}

pub(crate) async fn add_card(
    State(AppState { pool }): State<AppState>,
    data: Json<CardParams>,
) -> Result<&'static str, StatusCode> {
    dbg!(data);
    let ret: Vec<Identified<Card>> = pool
        .create("card")
        .content(Card {
            title: "Test".to_string(),
            // image: Some(vec![0, 1, 2, 3, 4].into()),
            text: None,
        })
        .await
        .map_err(|err| {
            warn!("{}", err);
            StatusCode::INTERNAL_SERVER_ERROR
        })?;

    dbg!(ret);

    Ok("OK")
}
