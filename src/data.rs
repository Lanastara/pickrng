use serde::{Deserialize, Serialize};
use surrealdb::iam::Resource;
use surrealdb::opt::RecordId;
use surrealdb::sql::Bytes;

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Identified<T> {
    id: RecordId,
    #[serde(flatten)]
    inner: T,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Card {
    pub title: String,
    // pub image: Option<Bytes>,
    pub text: Option<String>,
}
